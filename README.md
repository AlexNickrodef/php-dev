All routes tested through Postman.
In `resources\js\components\houses\list_with_actions.vue` you can see a list example with operation DELETE (action's column).

### Get started:

1. `composer install`
2. set up your db connection configs in .env
3. `php artisan migrate --seed`


After that refer to the project from web server path or run the command `php artisan serve`

![Example](https://i.ibb.co/pR1SFQt/example.gif)
