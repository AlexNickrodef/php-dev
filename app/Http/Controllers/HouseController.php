<?php

namespace App\Http\Controllers;

use App\House;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = $request->get('per_page') ? (integer)\request()->get('per_page') : House::DEFAULT_PAGINATION;

        $houses = House::query();

        // Sorting:

        if ($request->get('sort')) {
            $sort_type = 'asc';
            if ($request->get('sort_type') == 'descending') {
                $sort_type = 'desc';
            }
            $houses->orderBy($request->get('sort'), $sort_type);
        };

        // Filter properties:

        if ($request->get('name')) {
            $houses->where('name', 'like', '%' . $request->get('name') . '%');
        }

        if (is_array($request->get('price')) && $request->get('price')) {
            $houses->whereBetween('price', $request->get('price'));
        }

        if ($request->get('bedrooms')) {
            $houses->where('bedrooms', '=', $request->get('bedrooms'));
        }

        if ($request->get('bathrooms')) {
            $houses->where('bathrooms', '=', $request->get('bathrooms'));
        }

        if ($request->get('storeys')) {
            $houses->where('storeys', '=', $request->get('storeys'));
        }

        if ($request->get('garages')) {
            $houses->where('garages', '=', $request->get('garages'));
        }

        return response($houses->paginate($per_page), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response(['result' => 'ok', 'data' => $this->houseFields()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required'],
            'price' => ['required'],
            'bedrooms' => ['required'],
            'bathrooms' => ['required'],
            'storeys' => ['required'],
            'garages' => ['required'],
        ]);

        $status = 'ok';

        try {
            $house = House::create($data);
        } catch (\Exception $e) {
            $house = null;
            $status = 'fail';
        }

        return response(['status' => $status, 'data' => $house], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return array
     */
    public function show($id)
    {
        $house = null;
        $status = 'ok';

        try {
            $house = House::findOrFail($id);
        } catch (\Exception $e) {
            $status = 'fail';
        }

        return ['result' => $status, 'data' => $house];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response(['result' => 'ok', 'data' => $this->houseFields()], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = 'ok';

        try {
            $house = House::findOrFail($id);
            $house->update($request->all());
        } catch (\Exception $e) {
            $house = null;
            $status = 'fail';
        }

        return response(['status' => $status, 'data' => $house], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = 'ok';

        try {
            $house = House::findOrFail($id);
            $house->delete();
        } catch (\Exception $e) {
            $status = 'fail';
        }

        return response(['status' => $status, 'data' => null], 200);
    }

    /**
     * Returns an array of fill properties in House model
     *
     * @return array
     */
    public function houseFields()
    {
        $house = new House();
        return $house->getFillable();
    }
}
