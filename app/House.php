<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    const DEFAULT_PAGINATION = 5;

    protected $fillable = [
        'name',
        'price',
        'bedrooms',
        'bathrooms',
        'storeys',
        'garages'
    ];

    protected $casts = [
        'name' => 'string',
        'price' => 'integer',
        'bedrooms' => 'integer',
        'bathrooms' => 'integer',
        'storeys' => 'integer',
        'garages' => 'integer'
    ];

    public $timestamps = true;
}
