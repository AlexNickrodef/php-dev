<?php

use App\House;
use Illuminate\Database\Seeder;

class HouseSeeder extends Seeder
{
    /**
     * This seeder is based on the provided property-data.csv file
     * which is located in the csv folder in the public directory
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('/csv/property-data.csv');
        $file = fopen($path, 'r');

        $line = 0;
        while ($row = fgetcsv($file)) {

            $line++;

            // ignore the first line
            if ($line === 1) {
                continue;
            }

            House::create([
                'name' => $row[0],
                'price' => $row[1],
                'bedrooms' => $row[2],
                'bathrooms' => $row[3],
                'storeys' => $row[4],
                'garages' => $row[5]
            ]);
        }

    }
}
